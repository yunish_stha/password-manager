package com.freelance.password_manager.api.user.service;

import java.util.Map;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.freelance.password_manager.api.service.BaseService;
import com.freelance.password_manager.api.user.dto.UserRegistrationDto;
import com.freelance.password_manager.api.user.entity.User;

/**
 * @author yunish on 12/9/2020
 */
public interface UserService extends BaseService<User>, UserDetailsService {
    User getAuthenticated();

    User getByUsername(String username);

    User save(User user);

    User updatePassword(String username, String password);

    Map<Object, Object> userStatusCount();

    boolean checkIfValidOldPassword(User user, String password);

    User registerSave(UserRegistrationDto userRegistrationDto);
}
