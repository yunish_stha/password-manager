package com.freelance.password_manager.api.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.freelance.password_manager.api.user.entity.User;
import com.freelance.password_manager.core.enums.Status;

/**
 * @author yunish on 12/9/2020
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    User getUserByUsername(String username);

    User getUsersByUsernameAndStatus(String username, Status active);
}
