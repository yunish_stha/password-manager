package com.freelance.password_manager.api.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.freelance.password_manager.core.enums.Status;


/**
 * @author yunish on 12/13/2020
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRegistrationDto {

    private String name;
    private String username;
    private String email;
    private String password;
    private Status status;
}
