package com.freelance.password_manager.api.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author yunish on 12/9/2020
 */
public interface BaseService<T> {

    /**
     *
     */
    List<T> findAll();

    /**
     *
     */
    T findOne(Long id);

    /**
     *
     */
    T save(T t);

    /**
     *
     */
    Page<T> findAllPageable(Object t, Pageable pageable);

    /**
     *
     */
    List<T> saveAll(List<T> list);
}
