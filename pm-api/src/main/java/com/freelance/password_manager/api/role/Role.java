package com.freelance.password_manager.api.role;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import com.freelance.password_manager.core.entity.BaseEntity;


/**
 * @author yunish on 12/13/2020
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "role")
public class Role extends BaseEntity<Long> {
    private Long id;
    private String name;

    public Role(String name) {
        this.name = name;
    }
}
