package com.freelance.password_manager.api.password.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.freelance.password_manager.api.password.entity.Password;

/**
 * @author yunish on 1/8/2021
 */
public class PasswordServiceImpl implements PasswordService {

    @Override
    public List<Password> findAll() {
        return null;
    }

    @Override
    public Password findOne(Long id) {
        return null;
    }

    @Override
    public Password save(Password password) {
        return null;
    }

    @Override
    public Page<Password> findAllPageable(Object t, Pageable pageable) {
        return null;
    }

    @Override
    public List<Password> saveAll(List<Password> list) {
        return null;
    }
}
