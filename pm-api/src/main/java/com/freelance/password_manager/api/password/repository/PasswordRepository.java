package com.freelance.password_manager.api.password.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.freelance.password_manager.api.password.entity.Password;

/**
 * @author yunish on 1/8/2021
 */
public interface PasswordRepository extends JpaRepository<Password, Long>,
    JpaSpecificationExecutor<Password> {

    Password getPasswordByName(String name);
}
