package com.freelance.password_manager.api.password.entity;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import com.freelance.password_manager.api.user.entity.User;
import com.freelance.password_manager.core.entity.BaseEntity;

/**
 * @author yunish on 1/8/2021
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "password")
public class Password extends BaseEntity<Long> {
    private String name;
    private String URL;
    private String username;
    private String site_password;
}
