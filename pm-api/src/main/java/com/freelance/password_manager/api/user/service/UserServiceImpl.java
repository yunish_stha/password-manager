package com.freelance.password_manager.api.user.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.freelance.password_manager.api.role.Role;
import com.freelance.password_manager.api.user.dto.UserRegistrationDto;
import com.freelance.password_manager.api.user.entity.User;
import com.freelance.password_manager.api.user.repository.UserRepository;

/**
 * @author yunish on 12/9/2020
 */
@Service("userDetailService")
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public UserServiceImpl(
        @Autowired UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findOne(Long id) {
        return userRepository.getOne(id);
    }

    @Override
    public User save(User user) {
        return null;
    }

    @Override
    public User updatePassword(String username, String password) {
        return null;
    }

    @Override
    public Map<Object, Object> userStatusCount() {
        return null;
    }

    @Override
    public boolean checkIfValidOldPassword(User user, String password) {
        return false;
    }

    @Override
    public User registerSave(UserRegistrationDto userRegistrationDto) {
        User user = new User(userRegistrationDto.getName(), userRegistrationDto.getUsername(),
            userRegistrationDto.getEmail(), passwordEncoder.encode(userRegistrationDto.getPassword()),
            userRegistrationDto.getStatus(),
            Arrays.asList(new Role("ROLE_USER")), null); // Null might encounter error..
        return userRepository.save(user);
    }

    @Override
    public Page<User> findAllPageable(Object t, Pageable pageable) {
        return null;
    }

    @Override
    public List<User> saveAll(List<User> list) {
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.getUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(),
            user.getPassword(), mapRolesToAuthorities(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName()))
            .collect(Collectors.toList());
    }

    @Override
    public User getAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof UserDetails) {
            User user = (User) authentication.getPrincipal();
            user = this.getByUsername(user.getUsername());
            return user;
        } else {
            throw new UsernameNotFoundException(
                "User is not authenticated; Found " + " of type " + authentication.getPrincipal()
                    .getClass() + "; Expected type User");
        }

    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }

}
