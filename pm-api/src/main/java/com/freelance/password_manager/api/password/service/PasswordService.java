package com.freelance.password_manager.api.password.service;

import com.freelance.password_manager.api.password.entity.Password;
import com.freelance.password_manager.api.service.BaseService;

/**
 * @author yunish on 1/8/2021
 */
public interface PasswordService extends BaseService<Password> {

    Password save(Password password);

}
