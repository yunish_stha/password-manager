package com.freelance.password_manager.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freelance.password_manager.api.user.dto.UserRegistrationDto;
import com.freelance.password_manager.api.user.service.UserService;

/**
 * @author yunish on 12/15/2020
 */
@Controller
@RequestMapping(value = "/v1/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(
        UserService userService
    ) {
        this.userService = userService;
    }

//    @GetMapping(path = "/authenticated")
//    public ResponseEntity<?> getAuthenticated() {
//        return new RestResponseDto().success(userService.getAuthenticated());
//    }

//    @PostMapping
//    public ResponseEntity<?> saveUser(@RequestBody User user) {
//        return new RestResponseDto().success(userService.save(user));
//    }
@ModelAttribute("user")
public UserRegistrationDto userRegistrationDto() {
    return new UserRegistrationDto();
}

    @GetMapping("/register")
    public String showRegistrationForm() {
        return "registration";
    }

    @PostMapping(path = "/registration")
    public String registerUser(@ModelAttribute("user") UserRegistrationDto registrationDto) {
        userService.registerSave(registrationDto);
        return "redirect:/v1/user/register?success";
    }


}
