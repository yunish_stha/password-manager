package com.freelance.password_manager.main;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author yunish on 12/25/2020
 */
@Controller
public class MainController {
    @GetMapping(path = "/login")
    public String login() {
        return "login";
    }
    @GetMapping(path = "/")
    public String home() {
        return "index";
    }
}
