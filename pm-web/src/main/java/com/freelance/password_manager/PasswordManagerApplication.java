package com.freelance.password_manager;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@SpringBootApplication(scanBasePackages = "com.freelance.*")
@EnableJpaRepositories(basePackages = "com.freelance.*")
@EntityScan(basePackages = "com.freelance.*")
public class PasswordManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PasswordManagerApplication.class, args);
//        ClassLoader cl = ClassLoader.getSystemClassLoader();
//        URL[] urls = ((URLClassLoader)cl).getURLs();
//        Arrays.stream(urls).filter(u-> !u.getFile().contains(".jar")).forEach(u-> System.out
//            .println(u.getPath()));
    }

}
