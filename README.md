# Password Manager

A personal password manager for individuals who don't trust enterprise applications to store their credentials.