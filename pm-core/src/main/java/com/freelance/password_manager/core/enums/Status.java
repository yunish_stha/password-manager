package com.freelance.password_manager.core.enums;

/**
 * @author yunish on 12/9/2020
 */
public enum Status {
    INACTIVE("Inactive"), ACTIVE("Active"), DELETED("Deleted");

    private final String value;

    Status(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
