package com.freelance.password_manager.core.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * @author yunish on 12/9/2020
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonInclude(Include.NON_NULL)
public class RestResponseDto {
    private String message;
    private Object detail;

    @JsonIgnore
    private int code;

    public ResponseEntity success(Object obj) {
        RestResponseDto rrd = new RestResponseDto();
        rrd.setDetail(obj);
        rrd.setMessage("SUCCESS");
        return new ResponseEntity(rrd, HttpStatus.OK);
    }

    public ResponseEntity failure(String msg) {
        RestResponseDto rrd = new RestResponseDto();
        rrd.setMessage(msg);
        return new ResponseEntity(rrd, HttpStatus.BAD_REQUEST);
    }
}
